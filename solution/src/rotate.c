#include "rotate.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const* img ) {
    // Create a new image with the dimensions reversed
    struct image rotated = alloc_image(img->height, img->width);

    // Rotate the image 90 degrees counter-clockwise
    for (size_t y = 0; y < img->height; y++) {
        for (size_t x = 0; x < img->width; x++) {
            size_t newX = img->height - y - 1;
            size_t newY = x;
            rotated.data[newY * rotated.width + newX] = img->data[y * img->width + x];
        }
    }

    return rotated;
}
