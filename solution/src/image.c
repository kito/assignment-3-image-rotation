#include "image.h"

size_t calc_padding(size_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

struct image alloc_image(size_t width, size_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
    return img;
}

void free_image(struct image* img) {
    free(img->data);
    img->data = NULL;
}
