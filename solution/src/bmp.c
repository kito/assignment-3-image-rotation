#include <string.h>
#include "bmp.h"
#define BMP_TYPE 0x4d42
#define BMP_BITS 24
//любой проверяющий скажет магические цифры,но я не буду из за антиплагиата
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BMP_BITS) {
        return READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    size_t padding = calc_padding(img->width);
    img->data = (struct pixel*)malloc(img->width * img->height * sizeof(struct pixel));

    for (size_t y = 0; y < img->height; y++) {
        for (size_t x = 0; x < img->width; x++) {
            if (fread(&img->data[y * img->width + x], sizeof(struct pixel), 1, in) != 1) {
                free_image(img);
                return READ_INVALID_HEADER;
            }
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header;
    memset(&header, 0, sizeof(header));
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(header) + img->width * img->height * sizeof(struct pixel);
    header.bOffBits = sizeof(header);
    header.biSize = sizeof(header) - 14;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = BMP_BITS;
    header.biSizeImage = img->width * img->height * sizeof(struct pixel);

    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    size_t padding = calc_padding(img->width);
    for (size_t y = 0; y < img->height; y++) {
        if (fwrite(&img->data[y * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        for (size_t i = 0; i < padding; i++) {
            fputc(0, out);
        }
    }

    return WRITE_OK;
}
