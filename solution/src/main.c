#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>

int main(int argc, char* argv[]) {
    if (argc < 3) {
        printf("Usage: %s <source-image> <transformed-image>\n", argv[0]);
        return 1;
    }

    FILE* in = fopen(argv[1], "rb");
    if (!in) {
        printf("Failed to open input file\n");
        return 1;
    }

    struct image img;
    enum read_status status = from_bmp(in, &img);
    fclose(in);
    if (status != READ_OK) {
        printf("Failed to read input file\n");

    }

    struct image rotated = rotate(&img);
    free_image(&img);


    FILE* out = fopen(argv[2], "wb");
    if (!out) {
        printf("Failed to open output file\n");
        free_image(&rotated);
        return 1;
    }


    if (to_bmp(out, &rotated) != WRITE_OK) {
        printf("Failed to write output file\n");
        return 1;
    }
    fclose(out);
    free_image(&rotated);
    return 0;
}
